class SYNDIKAT_PARA
{
    #include "assets_templates.hpp"
    #include "roles_templates.hpp"
    class ZDB_ASSETS
    {
        #include "assets.hpp"
        #include "assets_forts.hpp"
    };
    class ZDB_GEAR
    {
        #include "gear_templates.hpp"
        #include "gear_restricted_items.hpp"
        #include "gear_arsenal.hpp"
    };
    class ZDB_ROLES
    {
        #include "roles_danger_items.hpp"
        #include "roles_loadouts.hpp"
    };
};
