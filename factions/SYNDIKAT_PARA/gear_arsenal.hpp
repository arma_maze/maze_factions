// Arsenal
 class WEAPONS
 {

  // Item :
  class Binocular                              {};  // Item , Binocular
  class Rangefinder                            {};  // Item , Binocular

  // Weapon : Handgun
  class hgun_Pistol_01_F                       {};  // Weapon , Handgun
  // Weapon : AssaultRifle
  class arifle_AK12_F                          {};  // Weapon , AssaultRifle
  class arifle_AK12_GL_F                       {};  // Weapon , AssaultRifle
  class arifle_AKM_F                           {};  // Weapon , AssaultRifle
  class arifle_AKS_F                           {};  // Weapon , AssaultRifle
  class arifle_CTAR_ghex_F                     {};  // Weapon , AssaultRifle
  class arifle_CTAR_GL_blk_F                   {};  // Weapon , AssaultRifle
  class arifle_CTAR_GL_ghex_F                  {};  // Weapon , AssaultRifle
  class arifle_CTAR_GL_hex_F                   {};  // Weapon , AssaultRifle
  class arifle_Katiba_F                        {};  // Weapon , AssaultRifle
  class arifle_Katiba_GL_F                     {};  // Weapon , AssaultRifle
  class arifle_Mk20_GL_plain_F                 {};  // Weapon , AssaultRifle
  class arifle_Mk20_plain_F                    {};  // Weapon , AssaultRifle
  class hgun_PDW2000_snds_F                    {};  // Weapon , AssaultRifle
  // Weapon : BombLauncher
  // Weapon : Cannon
  // Weapon : GrenadeLauncher
  // Weapon : Launcher
  // Weapon : MachineGun
  class LMG_03_F                               {};  // Weapon , MachineGun
  class LMG_Mk200_F                            {};  // Weapon , MachineGun
  // Weapon : Magazine
  // Weapon : MissileLauncher
  // Weapon : Mortar
  // Weapon : RocketLauncher
  class launch_RPG7_F                          {};  // Weapon , RocketLauncher
  // Weapon : Shotgun
  // Weapon : Throw
  // Weapon : Rifle
  // Weapon : SubmachineGun
  class SMG_01_F                               {};  // Weapon , SubmachineGun
  class SMG_05_F                               {};  // Weapon , SubmachineGun
  // Weapon : SniperRifle
  class srifle_DMR_01_F                        {};  // Weapon , SniperRifle
  class srifle_DMR_06_camo_F                   {};  // Weapon , SniperRifle
  class srifle_DMR_06_camo_khs_F               {};  // Weapon , SniperRifle
  class srifle_DMR_06_olive_F                  {};  // Weapon , SniperRifle

 };
 class MAGAZINES
 {
  class 10Rnd_762x54_Mag                       {};  // Magazine , Bullet
  class 10Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 16Rnd_9x21_green_Mag                   {};  // Magazine , Bullet
  class 16Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 16Rnd_9x21_red_Mag                     {};  // Magazine , Bullet
  class 16Rnd_9x21_yellow_Mag                  {};  // Magazine , Bullet
  class 1Rnd_HE_Grenade_shell                  {};  // Magazine , Shell
  class 200Rnd_556x45_Box_F                    {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Red_F                {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Tracer_F             {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Tracer_Red_F         {};  // Magazine , Bullet
  class 200Rnd_65x39_cased_Box                 {};  // Magazine , Bullet
  class 200Rnd_65x39_cased_Box_Tracer          {};  // Magazine , Bullet
  class 20Rnd_762x51_Mag                       {};  // Magazine , Bullet
  class 30Rnd_45ACP_Mag_SMG_01                 {};  // Magazine , Bullet
  class 30Rnd_45ACP_Mag_SMG_01_tracer_green    {};  // Magazine , Bullet
  class 30Rnd_45ACP_Mag_SMG_01_Tracer_Red      {};  // Magazine , Bullet
  class 30Rnd_45ACP_Mag_SMG_01_Tracer_Yellow   {};  // Magazine , Bullet
  class 30Rnd_545x39_Mag_F                     {};  // Magazine , Bullet
  class 30Rnd_545x39_Mag_Green_F               {};  // Magazine , Bullet
  class 30Rnd_545x39_Mag_Tracer_F              {};  // Magazine , Bullet
  class 30Rnd_545x39_Mag_Tracer_Green_F        {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag                    {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_green              {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_red                {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_Tracer_Green       {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_Tracer_Red         {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_Tracer_Yellow      {};  // Magazine , Bullet
  class 30Rnd_580x42_Mag_F                     {};  // Magazine , Bullet
  class 30Rnd_580x42_Mag_Tracer_F              {};  // Magazine , Bullet
  class 30Rnd_65x39_caseless_green             {};  // Magazine , Bullet
  class 30Rnd_65x39_caseless_green_mag_Tracer  {};  // Magazine , Bullet
  class 30Rnd_762x39_Mag_F                     {};  // Magazine , Bullet
  class 30Rnd_762x39_Mag_Green_F               {};  // Magazine , Bullet
  class 30Rnd_762x39_Mag_Tracer_F              {};  // Magazine , Bullet
  class 30Rnd_762x39_Mag_Tracer_Green_F        {};  // Magazine , Bullet
  class 30Rnd_9x21_Green_Mag                   {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag_SMG_02                  {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag_SMG_02_Tracer_Green     {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag_SMG_02_Tracer_Red       {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag_SMG_02_Tracer_Yellow    {};  // Magazine , Bullet
  class 30Rnd_9x21_Red_Mag                     {};  // Magazine , Bullet
  class 30Rnd_9x21_Yellow_Mag                  {};  // Magazine , Bullet
  class APERSBoundingMine_Range_Mag            {};  // Mine , MineBounding
  class APERSMine_Range_Mag                    {};  // Mine , Mine
  class APERSTripMine_Wire_Mag                 {};  // Mine , MineDirectional
  class ATMine_Range_Mag                       {};  // Mine , Mine
  class DemoCharge_Remote_Mag                  {};  // Mine , Mine
  class HandGrenade                            {};  // Magazine , Grenade
  class IEDLandBig_Remote_Mag                  {};  // Mine , Mine
  class IEDLandSmall_Remote_Mag                {};  // Mine , Mine
  class IEDUrbanBig_Remote_Mag                 {};  // Mine , Mine
  class IEDUrbanSmall_Remote_Mag               {};  // Mine , Mine
  class MiniGrenade                            {};  // Magazine , Grenade
  class RPG7_F                                 {};  // Magazine , Rocket
  class SatchelCharge_Remote_Mag               {};  // Mine , Mine
  class SmokeShell                             {};  // Magazine , SmokeShell
  class SmokeShellGreen                        {};  // Magazine , SmokeShell

 };
 class MAGAZINES
 {
  class 10Rnd_762x54_Mag                       {};  // Magazine , Bullet
  class 10Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 16Rnd_9x21_green_Mag                   {};  // Magazine , Bullet
  class 16Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 16Rnd_9x21_red_Mag                     {};  // Magazine , Bullet
  class 16Rnd_9x21_yellow_Mag                  {};  // Magazine , Bullet
  class 1Rnd_HE_Grenade_shell                  {};  // Magazine , Shell
  class 200Rnd_556x45_Box_F                    {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Red_F                {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Tracer_F             {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Tracer_Red_F         {};  // Magazine , Bullet
  class 200Rnd_65x39_cased_Box                 {};  // Magazine , Bullet
  class 200Rnd_65x39_cased_Box_Tracer          {};  // Magazine , Bullet
  class 20Rnd_762x51_Mag                       {};  // Magazine , Bullet
  class 30Rnd_45ACP_Mag_SMG_01                 {};  // Magazine , Bullet
  class 30Rnd_45ACP_Mag_SMG_01_tracer_green    {};  // Magazine , Bullet
  class 30Rnd_45ACP_Mag_SMG_01_Tracer_Red      {};  // Magazine , Bullet
  class 30Rnd_45ACP_Mag_SMG_01_Tracer_Yellow   {};  // Magazine , Bullet
  class 30Rnd_545x39_Mag_F                     {};  // Magazine , Bullet
  class 30Rnd_545x39_Mag_Green_F               {};  // Magazine , Bullet
  class 30Rnd_545x39_Mag_Tracer_F              {};  // Magazine , Bullet
  class 30Rnd_545x39_Mag_Tracer_Green_F        {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag                    {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_green              {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_red                {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_Tracer_Green       {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_Tracer_Red         {};  // Magazine , Bullet
  class 30Rnd_556x45_Stanag_Tracer_Yellow      {};  // Magazine , Bullet
  class 30Rnd_580x42_Mag_F                     {};  // Magazine , Bullet
  class 30Rnd_580x42_Mag_Tracer_F              {};  // Magazine , Bullet
  class 30Rnd_65x39_caseless_green             {};  // Magazine , Bullet
  class 30Rnd_65x39_caseless_green_mag_Tracer  {};  // Magazine , Bullet
  class 30Rnd_762x39_Mag_F                     {};  // Magazine , Bullet
  class 30Rnd_762x39_Mag_Green_F               {};  // Magazine , Bullet
  class 30Rnd_762x39_Mag_Tracer_F              {};  // Magazine , Bullet
  class 30Rnd_762x39_Mag_Tracer_Green_F        {};  // Magazine , Bullet
  class 30Rnd_9x21_Green_Mag                   {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag_SMG_02                  {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag_SMG_02_Tracer_Green     {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag_SMG_02_Tracer_Red       {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag_SMG_02_Tracer_Yellow    {};  // Magazine , Bullet
  class 30Rnd_9x21_Red_Mag                     {};  // Magazine , Bullet
  class 30Rnd_9x21_Yellow_Mag                  {};  // Magazine , Bullet
  class APERSBoundingMine_Range_Mag            {};  // Mine , MineBounding
  class APERSMine_Range_Mag                    {};  // Mine , Mine
  class APERSTripMine_Wire_Mag                 {};  // Mine , MineDirectional
  class ATMine_Range_Mag                       {};  // Mine , Mine
  class DemoCharge_Remote_Mag                  {};  // Mine , Mine
  class HandGrenade                            {};  // Magazine , Grenade
  class IEDLandBig_Remote_Mag                  {};  // Mine , Mine
  class IEDLandSmall_Remote_Mag                {};  // Mine , Mine
  class IEDUrbanBig_Remote_Mag                 {};  // Mine , Mine
  class IEDUrbanSmall_Remote_Mag               {};  // Mine , Mine
  class MiniGrenade                            {};  // Magazine , Grenade
  class RPG7_F                                 {};  // Magazine , Rocket
  class SatchelCharge_Remote_Mag               {};  // Mine , Mine
  class SmokeShell                             {};  // Magazine , SmokeShell
  class SmokeShellGreen                        {};  // Magazine , SmokeShell

 };
 class ITEMS
 {
 // Weapon Items
  class optic_DMS                              {};  // Item , AccessorySights
  class optic_ERCO_blk_F                       {};  // Item , AccessorySights

 // Cargo Items
  class FirstAidKit                            {};  // Item , FirstAidKit
  class Medikit                                {};  // Item , Medikit
  class ToolKit                                {};  // Item , Toolkit

 // Linked Items
  class ItemCompass                            {};  // Item , Compass
  class ItemMap                                {};  // Item , Map
  class ItemRadio                              {};  // Item , Radio
  class ItemWatch                              {};  // Item , Watch

 // Outfits
  class H_Bandanna_gry                         {};  // Equipment , Headgear
  class H_Booniehat_khk_hs                     {};  // Equipment , Headgear
  class H_Booniehat_tan                        {};  // Equipment , Headgear
  class H_MilCap_ocamo                         {};  // Equipment , Headgear
  class H_MilCap_tna_F                         {};  // Equipment , Headgear
  class H_Watchcap_camo                        {};  // Equipment , Headgear
  class U_I_C_Soldier_Camo_F                   {};  // Equipment , Uniform
  class U_I_C_Soldier_Para_1_F                 {};  // Equipment , Uniform
  class U_I_C_Soldier_Para_2_F                 {};  // Equipment , Uniform
  class U_I_C_Soldier_Para_3_F                 {};  // Equipment , Uniform
  class U_I_C_Soldier_Para_4_F                 {};  // Equipment , Uniform
  class U_I_C_Soldier_Para_5_F                 {};  // Equipment , Uniform
  class V_Chestrig_blk                         {};  // Equipment , Vest
  class V_Chestrig_khk                         {};  // Equipment , Vest
  class V_Chestrig_oli                         {};  // Equipment , Vest
  class V_TacChestrig_grn_F                    {};  // Equipment , Vest
  class V_TacVest_oli                          {};  // Equipment , Vest

 };
 class GLASSES
 {
  class G_Bandanna_oli                         {};  // Equipment , Glasses
  class G_Shades_Blue                          {};  // Equipment , Glasses
  class G_Sport_Blackred                       {};  // Equipment , Glasses

 };
 class BACKPACKS
 {
  class B_AssaultPack_tna_F                    {};  // Equipment , Backpack
  class B_FieldPack_ghex_F                     {};  // Equipment , Backpack
  class B_FieldPack_oli                        {};  // Equipment , Backpack
  class B_Kitbag_cbr                           {};  // Equipment , Backpack
  class B_Kitbag_rgr                           {};  // Equipment , Backpack

 };
