// I_C_Soldier_Para_1_F
class arifle_AK12_F                           { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Weapon - AssaultRifle
class arifle_AKM_F                            { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Weapon - AssaultRifle
class arifle_AKS_F                            { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Weapon - AssaultRifle
class arifle_CTAR_ghex_F                      { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Weapon - AssaultRifle
class arifle_Katiba_F                         { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Weapon - AssaultRifle
class arifle_Mk20_plain_F                     { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Weapon - AssaultRifle
class hgun_PDW2000_snds_F                     { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Weapon - AssaultRifle
class SMG_05_F                                { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Weapon - SubmachineGun
class SMG_01_F                                { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Weapon - SubmachineGun
class MiniGrenade                             { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  class max { id = "MiniGrenade"; max = 4; }; }; // Magazine - Grenade
class HandGrenade                             { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  class max { id = "HandGrenade"; max = 4; }; }; // Magazine - Grenade
class IEDLandSmall_Remote_Mag                 { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Mine - Mine
class IEDUrbanSmall_Remote_Mag                { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  }; // Mine - Mine
class SmokeShell                              { class roles : { roles[] = { I_C_Soldier_Para_1_F }; };  class max { id = "SmokeShell"; max = 5; }; }; // Magazine - SmokeShell
// I_C_Soldier_Para_6_F
class arifle_AK12_GL_F                        { class roles : { roles[] = { I_C_Soldier_Para_6_F }; };  }; // Weapon - AssaultRifle
class arifle_CTAR_GL_blk_F                    { class roles : { roles[] = { I_C_Soldier_Para_6_F }; };  }; // Weapon - AssaultRifle
class arifle_CTAR_GL_ghex_F                   { class roles : { roles[] = { I_C_Soldier_Para_6_F }; };  }; // Weapon - AssaultRifle
class arifle_CTAR_GL_hex_F                    { class roles : { roles[] = { I_C_Soldier_Para_6_F }; };  }; // Weapon - AssaultRifle
class arifle_Katiba_GL_F                      { class roles : { roles[] = { I_C_Soldier_Para_6_F }; };  }; // Weapon - AssaultRifle
class arifle_Mk20_GL_plain_F                  { class roles : { roles[] = { I_C_Soldier_Para_6_F }; };  }; // Weapon - AssaultRifle
// I_C_Soldier_Para_4_F
class LMG_03_F                                { class roles : { roles[] = { I_C_Soldier_Para_4_F }; };  }; // Weapon - MachineGun
class LMG_Mk200_F                             { class roles : { roles[] = { I_C_Soldier_Para_4_F }; };  }; // Weapon - MachineGun
class optic_ACO_grn                           { class roles : { roles[] = { I_C_Soldier_Para_4_F }; };  }; // Item - AccessorySights
class bipod_03_F_blk                          { class roles : { roles[] = { I_C_Soldier_Para_4_F }; };  }; // Item - AccessoryBipod
class bipod_03_F_oli                          { class roles : { roles[] = { I_C_Soldier_Para_4_F }; };  }; // Item - AccessoryBipod
// I_C_Soldier_Para_8_F
class APERSBoundingMine_Range_Mag             { class roles : { roles[] = { I_C_Soldier_Para_8_F }; };  class max { id = "APERSBoundingMine_Range_Mag"; max = 3; }; }; // Mine - MineBounding
class APERSMine_Range_Mag                     { class roles : { roles[] = { I_C_Soldier_Para_8_F }; };  class max { id = "APERSMine_Range_Mag"; max = 2; }; }; // Mine - Mine
class APERSTripMine_Wire_Mag                  { class roles : { roles[] = { I_C_Soldier_Para_8_F }; };  class max { id = "APERSTripMine_Wire_Mag"; max = 3; }; }; // Mine - MineDirectional
class ATMine_Range_Mag                        { class roles : { roles[] = { I_C_Soldier_Para_8_F }; };  class max { id = "ATMine_Range_Mag"; max = 2; }; }; // Mine - Mine
class DemoCharge_Remote_Mag                   { class roles : { roles[] = { I_C_Soldier_Para_8_F }; };  class max { id = "DemoCharge_Remote_Mag"; max = 2; }; }; // Mine - Mine
class SatchelCharge_Remote_Mag                { class roles : { roles[] = { I_C_Soldier_Para_8_F }; };  }; // Mine - Mine
class IEDLandBig_Remote_Mag                   { class roles : { roles[] = { I_C_Soldier_Para_8_F }; };  class max { id = "IEDLandBig_Remote_Mag"; max = 2; }; }; // Mine - Mine
class IEDUrbanBig_Remote_Mag                  { class roles : { roles[] = { I_C_Soldier_Para_8_F }; };  class max { id = "IEDUrbanBig_Remote_Mag"; max = 2; }; }; // Mine - Mine
// I_C_Soldier_Para_3_F
class Medikit                                 { class roles : { roles[] = { I_C_Soldier_Para_3_F }; };  }; // Item - Medikit
// I_C_Soldier_Para_5_F
class launch_RPG7_F                           { class roles : { roles[] = { I_C_Soldier_Para_5_F }; };  }; // Weapon - RocketLauncher
class RPG7_F                                  { class roles : { roles[] = { I_C_Soldier_Para_5_F }; };  class max { id = "RPG7_F"; max = 2; }; }; // Magazine - Rocket
// I_C_Soldier_Para_2_F
class srifle_DMR_06_camo_F                    { class roles : { roles[] = { I_C_Soldier_Para_2_F }; };  }; // Weapon - SniperRifle
class srifle_DMR_06_camo_khs_F                { class roles : { roles[] = { I_C_Soldier_Para_2_F }; };  }; // Weapon - SniperRifle
class srifle_DMR_06_olive_F                   { class roles : { roles[] = { I_C_Soldier_Para_2_F }; };  }; // Weapon - SniperRifle
class srifle_DMR_01_F                         { class roles : { roles[] = { I_C_Soldier_Para_2_F }; };  }; // Weapon - SniperRifle
class Rangefinder                             { class roles : { roles[] = { I_C_Soldier_Para_2_F }; };  }; // Item - Binocular
class optic_DMS                               { class roles : { roles[] = { I_C_Soldier_Para_2_F }; };  }; // Item - AccessorySights
class optic_AMS_snd                           { class roles : { roles[] = { I_C_Soldier_Para_2_F }; };  }; // Item - AccessorySights
class optic_ERCO_snd_F                        { class roles : { roles[] = { I_C_Soldier_Para_2_F }; };  }; // Item - AccessorySights
// 
class Binocular                               { class roles : { roles[] = {  }; };  }; // Item - Binocular
class ItemGPS                                 { class roles : { roles[] = {  }; };  }; // Item - GPS
class I_UavTerminal                           { class roles : { roles[] = {  }; };  }; // Item - UAVTerminal
