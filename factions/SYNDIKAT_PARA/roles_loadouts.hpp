// Roles Classes

  class I_C_Soldier_Para_1_F// I_C_Soldier_Para_1_F : R Alpha 3-3:1
  {
    name = "Rifleman"; // I_C_Soldier_Para_1_F : Onaona Koroi : WEST : SYNDIKAT_PARA : 
    marker_symbol = "b_inf"; // iconMan : \A3\ui_f\data\map\vehicleicons\iconMan_ca.paa 
    class category { roles[] = { I_C_Soldier_Para_1_F }; max[]= { 100 }; }; 
    weapons               [] = { "arifle_AK12_F" };
    uniform_items         [] = { "FirstAidKit" };
    vest_items            [] = { "30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","HandGrenade","HandGrenade" };
    backpack_items        [] = { "" };
    primary_weapon_items  [] = { "","","","" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "30Rnd_762x39_Mag_F" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "" };
    body_items            [] = { "FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio" };
    uniform                  = "U_I_C_Soldier_Para_4_F";
    vest                     = "V_Chestrig_khk";
    backpack                 = "";
    headgear                 = "";
    goggles                  = "";
    binocular                = "";
  };

  class I_C_Soldier_Para_6_F// I_C_Soldier_Para_6_F : R Alpha 3-1:1
  {
    name = "Grenadier"; // I_C_Soldier_Para_6_F : Tangaroa Bole : WEST : SYNDIKAT_PARA : 
    marker_symbol = "b_inf"; // iconMan : \A3\ui_f\data\map\vehicleicons\iconMan_ca.paa 
    class category { roles[] = { I_C_Soldier_Para_6_F }; max[]= { {1,10,20},(1,2,4} }; }; 
    weapons               [] = { "arifle_AK12_GL_F" };
    uniform_items         [] = { "FirstAidKit" };
    vest_items            [] = { "30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell" };
    backpack_items        [] = { "1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","1Rnd_HE_Grenade_shell","30Rnd_762x39_Mag_Tracer_Green_F","30Rnd_762x39_Mag_Tracer_Green_F","30Rnd_762x39_Mag_Tracer_Green_F","30Rnd_762x39_Mag_Tracer_Green_F","30Rnd_762x39_Mag_Tracer_Green_F","30Rnd_762x39_Mag_Tracer_Green_F" };
    primary_weapon_items  [] = { "","","","" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "30Rnd_762x39_Mag_F","1Rnd_HE_Grenade_shell" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "" };
    body_items            [] = { "FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio" };
    uniform                  = "U_I_C_Soldier_Para_1_F";
    vest                     = "V_Chestrig_oli";
    backpack                 = "B_FieldPack_ghex_F";
    headgear                 = "";
    goggles                  = "";
    binocular                = "";
  };

  class I_C_Soldier_Para_4_F// I_C_Soldier_Para_4_F : R Alpha 2-2:1
  {
    name = "Machine Gunner"; // I_C_Soldier_Para_4_F : Keon Karetu : WEST : SYNDIKAT_PARA : 
    marker_symbol = "b_inf"; // iconManMG : \A3\ui_f\data\map\vehicleicons\iconManMG_ca.paa 
    class category { roles[] = { I_C_Soldier_Para_4_F }; max[]= { 50 }; }; 
    weapons               [] = { "LMG_03_F","hgun_Pistol_01_F" };
    uniform_items         [] = { "FirstAidKit" };
    vest_items            [] = { "200Rnd_556x45_Box_F","200Rnd_556x45_Box_F" };
    backpack_items        [] = { "" };
    primary_weapon_items  [] = { "","","","" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "200Rnd_556x45_Box_F" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "10Rnd_9x21_Mag" };
    body_items            [] = { "FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio" };
    uniform                  = "U_I_C_Soldier_Para_4_F";
    vest                     = "V_Chestrig_blk";
    backpack                 = "";
    headgear                 = "";
    goggles                  = "";
    binocular                = "";
  };

  class I_C_Soldier_Para_8_F// I_C_Soldier_Para_8_F : R Alpha 4-1:1
  {
    name = "Sabouteur (Explosives)"; // I_C_Soldier_Para_8_F : Aluna Kingi : WEST : SYNDIKAT_PARA : 
    marker_symbol = "b_inf"; // iconManExplosive : \A3\ui_f\data\map\vehicleicons\iconManExplosive_ca.paa 
    class category { roles[] = { I_C_Soldier_Para_8_F }; max[]= { 50 }; }; 
    weapons               [] = { "arifle_AKM_F" };
    uniform_items         [] = { "FirstAidKit","ToolKit" };
    vest_items            [] = { "" };
    backpack_items        [] = { "ToolKit","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","DemoCharge_Remote_Mag","DemoCharge_Remote_Mag","DemoCharge_Remote_Mag","SatchelCharge_Remote_Mag" };
    primary_weapon_items  [] = { "","","","" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "30Rnd_762x39_Mag_F" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "" };
    body_items            [] = { "FirstAidKit","ToolKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio" };
    uniform                  = "U_I_C_Soldier_Para_2_F";
    vest                     = "";
    backpack                 = "B_Kitbag_rgr";
    headgear                 = "H_Booniehat_tan";
    goggles                  = "G_Bandanna_oli";
    binocular                = "";
  };

  class I_C_Soldier_Para_3_F// I_C_Soldier_Para_3_F : R Alpha 2-1:1
  {
    name = "Medic"; // I_C_Soldier_Para_3_F : Akamu Ganilau : WEST : SYNDIKAT_PARA : 
    marker_symbol = "b_inf"; // iconManMedic : \A3\ui_f\data\map\vehicleicons\iconManMedic_ca.paa 
    class category { roles[] = { I_C_Soldier_Para_3_F }; max[]= { 50 }; }; 
    weapons               [] = { "arifle_AKM_F" };
    uniform_items         [] = { "FirstAidKit","Medikit","FirstAidKit","FirstAidKit","FirstAidKit","FirstAidKit" };
    vest_items            [] = { "" };
    backpack_items        [] = { "Medikit","FirstAidKit","FirstAidKit","FirstAidKit","FirstAidKit","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F" };
    primary_weapon_items  [] = { "","","","" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "30Rnd_762x39_Mag_F" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "" };
    body_items            [] = { "FirstAidKit","Medikit","FirstAidKit","FirstAidKit","FirstAidKit","FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio" };
    uniform                  = "U_I_C_Soldier_Para_3_F";
    vest                     = "";
    backpack                 = "B_Kitbag_rgr";
    headgear                 = "H_Bandanna_gry";
    goggles                  = "G_Bandanna_oli";
    binocular                = "";
  };

  class I_C_Soldier_Para_5_F// I_C_Soldier_Para_5_F : R Alpha 1-1:1
  {
    name = "AT (RPG)"; // I_C_Soldier_Para_5_F : Atonio Lomu : WEST : SYNDIKAT_PARA : 
    marker_symbol = "b_inf"; // iconManAT : \A3\ui_f\data\map\vehicleicons\iconManAT_ca.paa 
    class category { roles[] = { I_C_Soldier_Para_5_F }; max[]= { 10 }; }; 
    weapons               [] = { "arifle_AKS_F","launch_RPG7_F" };
    uniform_items         [] = { "FirstAidKit" };
    vest_items            [] = { "" };
    backpack_items        [] = { "30Rnd_545x39_Mag_F","30Rnd_545x39_Mag_F","30Rnd_545x39_Mag_F","30Rnd_545x39_Mag_F","30Rnd_545x39_Mag_F","30Rnd_545x39_Mag_F","30Rnd_545x39_Mag_F","30Rnd_545x39_Mag_F","30Rnd_545x39_Mag_F","30Rnd_545x39_Mag_F","RPG7_F","RPG7_F","RPG7_F","RPG7_F","RPG7_F" };
    primary_weapon_items  [] = { "","","","" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "30Rnd_545x39_Mag_F" };
    secondary_weapon_mag  [] = { "RPG7_F" };
    handgun_mag           [] = { "" };
    body_items            [] = { "FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio" };
    uniform                  = "U_I_C_Soldier_Para_5_F";
    vest                     = "";
    backpack                 = "B_Kitbag_cbr";
    headgear                 = "H_MilCap_ocamo";
    goggles                  = "";
    binocular                = "";
  };

  class I_C_Soldier_Para_1_F// I_C_Soldier_Para_1_F : R Alpha 3-2:1
  {
    name = "Crewman"; // I_C_Soldier_Para_1_F : Ahonui Reihana : WEST : SYNDIKAT_PARA : 
    marker_symbol = "b_inf"; // iconMan : \A3\ui_f\data\map\vehicleicons\iconMan_ca.paa 
    class category { roles[] = { I_C_Soldier_Para_1_F }; max[]= { {1,10,20},(1,2,4} }; }; 
    weapons               [] = { "arifle_AKS_F" };
    uniform_items         [] = { "FirstAidKit" };
    vest_items            [] = { "HandGrenade","HandGrenade" };
    backpack_items        [] = { "" };
    primary_weapon_items  [] = { "","","","" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "30Rnd_545x39_Mag_F" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "" };
    body_items            [] = { "FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio" };
    uniform                  = "U_I_C_Soldier_Para_1_F";
    vest                     = "V_TacChestrig_grn_F";
    backpack                 = "B_AssaultPack_tna_F";
    headgear                 = "H_MilCap_tna_F";
    goggles                  = "G_Shades_Blue";
    binocular                = "";
  };

  class I_C_Soldier_Para_7_F// I_C_Soldier_Para_7_F : R Alpha 1-2:1
  {
    name = "Pilot"; // I_C_Soldier_Para_7_F : Etera Molia : WEST : SYNDIKAT_PARA : 
    marker_symbol = "b_inf"; // iconMan : \A3\ui_f\data\map\vehicleicons\iconMan_ca.paa 
    class category { roles[] = { I_C_Soldier_Para_7_F }; max[]= { {1,10,20},(1,2,4} }; }; 
    weapons               [] = { "arifle_AKM_F" };
    uniform_items         [] = { "FirstAidKit" };
    vest_items            [] = { "30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","HandGrenade","HandGrenade" };
    backpack_items        [] = { "" };
    primary_weapon_items  [] = { "","","","" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "30Rnd_762x39_Mag_F" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "" };
    body_items            [] = { "FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio" };
    uniform                  = "U_I_C_Soldier_Para_4_F";
    vest                     = "V_Chestrig_khk";
    backpack                 = "";
    headgear                 = "H_Booniehat_khk_hs";
    goggles                  = "G_Sport_Blackred";
    binocular                = "";
  };

  class I_C_Soldier_Para_2_F// I_C_Soldier_Para_2_F : R Alpha 1-3:1
  {
    name = "Enforcer - Marksman"; // I_C_Soldier_Para_2_F : Olina Veitayaki : WEST : SYNDIKAT_PARA : 
    marker_symbol = "b_inf"; // iconManLeader : \A3\ui_f\data\map\vehicleicons\iconManLeader_ca.paa 
    class category { roles[] = { I_C_Soldier_Para_2_F }; max[]= { {1,10,20},(1,1,2} }; }; 
    weapons               [] = { "arifle_AK12_F","hgun_Pistol_01_F" };
    uniform_items         [] = { "FirstAidKit","FirstAidKit","FirstAidKit" };
    vest_items            [] = { "30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","16Rnd_9x21_Mag","16Rnd_9x21_Mag","HandGrenade","HandGrenade" };
    backpack_items        [] = { "FirstAidKit","FirstAidKit","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","30Rnd_762x39_Mag_F","MiniGrenade","MiniGrenade","MiniGrenade","MiniGrenade","MiniGrenade","SmokeShellGreen","SmokeShellGreen","SmokeShellGreen","SmokeShellGreen","SmokeShellGreen" };
    primary_weapon_items  [] = { "","","optic_DMS","" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "30Rnd_762x39_Mag_F" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "16Rnd_9x21_Mag" };
    body_items            [] = { "FirstAidKit","FirstAidKit","FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio" };
    uniform                  = "U_I_C_Soldier_Para_2_F";
    vest                     = "V_TacVest_oli";
    backpack                 = "B_FieldPack_oli";
    headgear                 = "H_Watchcap_camo";
    goggles                  = "";
    binocular                = "";
  };
