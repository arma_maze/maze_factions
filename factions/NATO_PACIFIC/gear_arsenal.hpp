// Arsenal
 class WEAPONS
 {

  // Item :
  class Binocular                              {};  // Item , Binocular

  // Weapon : Handgun
  class hgun_P07_F                             {};  // Weapon , Handgun
  // Weapon : AssaultRifle
  // Weapon : BombLauncher
  // Weapon : Cannon
  // Weapon : GrenadeLauncher
  // Weapon : Launcher
  // Weapon : MachineGun
  class arifle_MX_SW_pointer_F                 {};  // Weapon , MachineGun
  class LMG_03_F                               {};  // Weapon , MachineGun
  class LMG_Mk200_BI_F                         {};  // Weapon , MachineGun
  class LMG_Mk200_F                            {};  // Weapon , MachineGun
  class LMG_Mk200_LP_BI_F                      {};  // Weapon , MachineGun
  class LMG_Mk200_pointer_F                    {};  // Weapon , MachineGun
  class MMG_02_sand_RCO_LP_F                   {};  // Weapon , MachineGun
  // Weapon : Magazine
  // Weapon : MissileLauncher
  // Weapon : Mortar
  // Weapon : RocketLauncher
  // Weapon : Shotgun
  // Weapon : Throw
  // Weapon : Rifle
  // Weapon : SubmachineGun
  // Weapon : SniperRifle

 };
 class MAGAZINES
 {
  class 100Rnd_65x39_caseless_mag              {};  // Magazine , Bullet
  class 100Rnd_65x39_caseless_mag_Tracer       {};  // Magazine , Bullet
  class 130Rnd_338_Mag                         {};  // Magazine , Bullet
  class 16Rnd_9x21_green_Mag                   {};  // Magazine , Bullet
  class 16Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 16Rnd_9x21_red_Mag                     {};  // Magazine , Bullet
  class 16Rnd_9x21_yellow_Mag                  {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_F                    {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Red_F                {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Tracer_F             {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Tracer_Red_F         {};  // Magazine , Bullet
  class 200Rnd_65x39_cased_Box                 {};  // Magazine , Bullet
  class 200Rnd_65x39_cased_Box_Tracer          {};  // Magazine , Bullet
  class 30Rnd_65x39_caseless_mag               {};  // Magazine , Bullet
  class 30Rnd_65x39_caseless_mag_Tracer        {};  // Magazine , Bullet
  class 30Rnd_9x21_Green_Mag                   {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 30Rnd_9x21_Red_Mag                     {};  // Magazine , Bullet
  class 30Rnd_9x21_Yellow_Mag                  {};  // Magazine , Bullet
  class Chemlight_green                        {};  // Magazine , SmokeShell
  class HandGrenade                            {};  // Magazine , Grenade
  class RPG32_F                                {};  // Magazine , Rocket
  class RPG32_HE_F                             {};  // Magazine , Rocket
  class SmokeShell                             {};  // Magazine , SmokeShell
  class SmokeShellBlue                         {};  // Magazine , SmokeShell
  class SmokeShellGreen                        {};  // Magazine , SmokeShell

 };
 class MAGAZINES
 {
  class 100Rnd_65x39_caseless_mag              {};  // Magazine , Bullet
  class 100Rnd_65x39_caseless_mag_Tracer       {};  // Magazine , Bullet
  class 130Rnd_338_Mag                         {};  // Magazine , Bullet
  class 16Rnd_9x21_green_Mag                   {};  // Magazine , Bullet
  class 16Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 16Rnd_9x21_red_Mag                     {};  // Magazine , Bullet
  class 16Rnd_9x21_yellow_Mag                  {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_F                    {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Red_F                {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Tracer_F             {};  // Magazine , Bullet
  class 200Rnd_556x45_Box_Tracer_Red_F         {};  // Magazine , Bullet
  class 200Rnd_65x39_cased_Box                 {};  // Magazine , Bullet
  class 200Rnd_65x39_cased_Box_Tracer          {};  // Magazine , Bullet
  class 30Rnd_65x39_caseless_mag               {};  // Magazine , Bullet
  class 30Rnd_65x39_caseless_mag_Tracer        {};  // Magazine , Bullet
  class 30Rnd_9x21_Green_Mag                   {};  // Magazine , Bullet
  class 30Rnd_9x21_Mag                         {};  // Magazine , Bullet
  class 30Rnd_9x21_Red_Mag                     {};  // Magazine , Bullet
  class 30Rnd_9x21_Yellow_Mag                  {};  // Magazine , Bullet
  class Chemlight_green                        {};  // Magazine , SmokeShell
  class HandGrenade                            {};  // Magazine , Grenade
  class RPG32_F                                {};  // Magazine , Rocket
  class RPG32_HE_F                             {};  // Magazine , Rocket
  class SmokeShell                             {};  // Magazine , SmokeShell
  class SmokeShellBlue                         {};  // Magazine , SmokeShell
  class SmokeShellGreen                        {};  // Magazine , SmokeShell

 };
 class ITEMS
 {
 // Weapon Items
  class acc_pointer_IR                         {};  // Item , AccessoryPointer
  class bipod_01_F_snd                         {};  // Item , AccessoryBipod
  class optic_Hamr                             {};  // Item , AccessorySights

 // Cargo Items
  class FirstAidKit                            {};  // Item , FirstAidKit

 // Linked Items
  class ItemCompass                            {};  // Item , Compass
  class ItemMap                                {};  // Item , Map
  class ItemRadio                              {};  // Item , Radio
  class ItemWatch                              {};  // Item , Watch
  class NVGoggles                              {};  // Item , NVGoggles

 // Outfits
  class H_HelmetB                              {};  // Equipment , Headgear
  class H_HelmetB_grass                        {};  // Equipment , Headgear
  class U_B_CombatUniform_mcam                 {};  // Equipment , Uniform
  class U_B_CombatUniform_mcam_tshirt          {};  // Equipment , Uniform
  class V_PlateCarrier1_rgr                    {};  // Equipment , Vest
  class V_PlateCarrier2_rgr                    {};  // Equipment , Vest

 };
 class GLASSES
 {
  class G_Combat                               {};  // Equipment , Glasses

 };
 class BACKPACKS
 {

 };
