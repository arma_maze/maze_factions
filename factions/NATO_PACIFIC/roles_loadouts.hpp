// Roles Classes

  class B_HeavyGunner_F// B_HeavyGunner_F : B Alpha 1-1:1
  {
    name = "Heavy Gunner"; // B_HeavyGunner_F : Wallace Johnson : WEST : NATO_PACIFIC : 
    marker_symbol = "b_inf"; // iconManMG : \A3\ui_f\data\map\vehicleicons\iconManMG_ca.paa 
    class category { roles[] = { B_HeavyGunner_F }; max[]= { {1,10,20},(1,2,4} }; }; 
    weapons               [] = { "MMG_02_sand_RCO_LP_F","hgun_P07_F" };
    uniform_items         [] = { "FirstAidKit" };
    vest_items            [] = { "130Rnd_338_Mag","130Rnd_338_Mag" };
    backpack_items        [] = { "" };
    primary_weapon_items  [] = { "","acc_pointer_IR","optic_Hamr","bipod_01_F_snd" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "130Rnd_338_Mag" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "16Rnd_9x21_Mag" };
    body_items            [] = { "FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles" };
    uniform                  = "U_B_CombatUniform_mcam";
    vest                     = "V_PlateCarrier1_rgr";
    backpack                 = "";
    headgear                 = "H_HelmetB";
    goggles                  = "G_Combat";
    binocular                = "";
    danger_items          [] = { "RPG32_HE_F","RPG32_F" }; 
  };

  class B_soldier_AR_F// B_soldier_AR_F : B Alpha 1-2:1
  {
    name = "Autorifleman"; // B_soldier_AR_F : Cameron Allen : WEST : NATO_PACIFIC : 
    marker_symbol = "b_inf"; // iconManMG : \A3\ui_f\data\map\vehicleicons\iconManMG_ca.paa 
    class category { roles[] = { B_soldier_AR_F }; max[]= { 10 }; }; 
    weapons               [] = { "arifle_MX_SW_pointer_F","hgun_P07_F" };
    uniform_items         [] = { "FirstAidKit" };
    vest_items            [] = { "100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag","16Rnd_9x21_Mag","16Rnd_9x21_Mag","Chemlight_green" };
    backpack_items        [] = { "" };
    primary_weapon_items  [] = { "","acc_pointer_IR","","bipod_01_F_snd" };
    secondary_weapon_items[] = { "","","","" };
    handgun_items         [] = { "","","","" };
    primary_weapon_mag    [] = { "100Rnd_65x39_caseless_mag" };
    secondary_weapon_mag  [] = { "" };
    handgun_mag           [] = { "16Rnd_9x21_Mag" };
    body_items            [] = { "FirstAidKit" };
    linked_items          [] = { "ItemMap","ItemCompass","ItemWatch","ItemRadio","NVGoggles" };
    uniform                  = "U_B_CombatUniform_mcam_tshirt";
    vest                     = "V_PlateCarrier2_rgr";
    backpack                 = "";
    headgear                 = "H_HelmetB_grass";
    goggles                  = "";
    binocular                = "";
    danger_items          [] = { "RPG32_HE_F","RPG32_F" }; 
  };
