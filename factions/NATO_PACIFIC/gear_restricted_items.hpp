// B_HeavyGunner_F
class Binocular                               { class roles : { roles[] = { B_HeavyGunner_F }; };  }; // Item - Binocular
class SmokeShellBlue                          { class roles : { roles[] = { B_HeavyGunner_F }; };  class max { id = "SmokeShellBlue"; max = 5; }; }; // Magazine - SmokeShell
class SmokeShell                              { class roles : { roles[] = { B_HeavyGunner_F }; };  class max { id = "SmokeShell"; max = 4; }; }; // Magazine - SmokeShell
class ItemGPS                                 { class roles : { roles[] = { B_HeavyGunner_F }; };  }; // Item - GPS
class I_UavTerminal                           { class roles : { roles[] = { B_HeavyGunner_F }; };  }; // Item - UAVTerminal
