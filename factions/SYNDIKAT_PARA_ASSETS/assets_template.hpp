#define _TC_ROLES_ _ZDB_R_TEAM_COORDINATOR_, _ZDB_R_RECON_TEAM_COORDINATOR_, _ZDB_R_COMMANDER_

    // assets templates
    //
    class ta_infantry
    {
        categories[] = {"car"};
        marker_symbol = "b_unknown";
        max[] = {{1, 8, 16}, {1, 2, 4}};
        cost = 1;
        empty_timeout = ZDB_CLEAN_ABANDONED;
        };
    
    class ta_infantry
    {
        categories[] = {"car"};
        marker_symbol = "b_unknown";
        max[] = {{1, 8, 16}, {1, 2, 4}};
        cost = 1;
        empty_timeout = ZDB_CLEAN_ABANDONED;
    };
    
    class ta_navy
    {
	    	categories[] = {"boat"};
        marker_symbol = "b_naval";
        max[] = {{1, 8, 16}, {1, 2, 4}};
        cost = ZDB_VEH_B_UNARMED;
        empty_timeout = ZDB_CLEAN_ABANDONED;
    };
    
    class ta_crewman
    {
        creation_roles[] = {_ZDB_R_CREWMAN_};
        usage_roles[] = {_ZDB_R_CREWMAN_};
        empty_timeout = ZDB_CLEAN_ABANDONED;
    };
    
    class ta_pilot
    {
        creation_roles[] = {_ZDB_R_PILOT_};
        usage_roles[] = {_ZDB_R_PILOT_};
        empty_timeout = ZDB_CLEAN_ABANDONED;
    };
    
    class ta_mrap : ta_crewman
    {
        categories[] = {"mrap"};
        max[] = {{1, 8, 16}, {0, 1, 1}};
        marker_symbol = "b_motor_inf";
        cost = ZDB_VEH_C_ARMED;
    };
    
    class ta_apc : ta_crewman
    {
        categories[] = {"apc"};
        max[] = {{1, 8, 16}, {0, 0, 1}};
        marker_symbol = "b_mech_inf";
        cost = ZDB_VEH_T_ARMED;
    service_time = 60;
    };
    
    class ta_tank : ta_crewman
    {
        categories[] = {"tank"};
        max[] = {{1, 8, 16}, {0, 0, 1}};
        marker_symbol = "b_armor";
        cost = ZDB_VEH_T_ATTACK;
        deploy_types = [[10, _ZDB_DCOND_BASE_, -1, _ZDB_DTYPE_MAPCLICK_]];
        service_time = 60;
    };
    class ta_heli_transport : ta_pilot
    {
        categories[] = {"heli_transport"};
        max[] = {{1, 8, 16}, {0, 1, 1}};
        deploy_types = [[10, _ZDB_DCOND_BASE_, -1, _ZDB_DTYPE_MAPCLICK_]];
        marker_symbol = "b_air";
        empty_timeout = -1;
    };
    class ta_heli_attack : ta_pilot
    {
        categories[] = {"heli_attack"};
        max[] = {{1, 8, 16}, {0, 1, 1}};
        deploy_types = [[10, _ZDB_DCOND_BASE_, -1, _ZDB_DTYPE_MAPCLICK_]];
        marker_symbol = "b_air";
        cost = ZDB_VEH_H_ATTACK;
        service_time = 60;
    };
    
    class ta_plane : ta_pilot
    {
        categories[] = {"plane"};
        max[] = {{1, 8, 16}, {0, 0, 1}};
        deploy_types = [[10, _ZDB_DCOND_BASE_, -1, _ZDB_DTYPE_MAPCLICK_]];
        marker_symbol = "b_plane";
        cost = ZDB_VEH_P_ATTACK;
        service_time = 60;
    };
    