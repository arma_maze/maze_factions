
// forts in sectors

    class bagbunker_small
    {
        type = "land_bagbunker_small_f";
        resize_box[] = {1, 1, 1, 1, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
        capabilities[] = {_ZDB_CAPABIL_FORT_};
        categories[] = {"fort"};
        max[] = {10};
        deploy_types = [[5, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_DTYPE_BOX_]];
        undeploy_types = [[5, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_UTYPE_NEARBY_]];
        deployable_code = "CUS_FNC_ASSET_DEPLOYABLE_FORT";
    };

    class bagfence_long : bagbunker_small
    {
        type = "land_bagfence_long_f";
        resize_box[] = {0.9, 1, 1, 0.9, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class bagfence_round : bagbunker_small
    {
        type = "land_bagfence_round_f";
        resize_box[] = {0.9, 0, 1, 0.9, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class bagfence_short : bagbunker_small
    {
        type = "land_bagfence_short_f";
        resize_box[] = {0.8, 1, 1, 0.8, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };


    // forts away from sectors

    class hbarrier_3 : bagbunker_small
    {
        type = "land_hbarrier_3_f";
        resize_box[] = {0.9, 1, 1, 0.9, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
        categories[] = {"fort_fob"};
        max[] = {50};
        deployable_code = "CUS_FNC_ASSET_DEPLOYABLE_FORT_FOB";
    };

    class hbarrier_5 : hbarrier_3
    {
        type = "land_hbarrier_5_f";
        resize_box[] = {0.9, 1, 1, 0.9, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class hbarrierbig : hbarrier_3
    {
        type = "land_hbarrierbig_f";
        resize_box[] = {0.9, 1, 1, 0.9, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class hbarrierwall : hbarrier_3
    {
        type = "land_hbarrierwall_corner_f";
        resize_box[] = {0.9, 0.65, 1, 0.9, 1, 0.6, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class hbarrierwall4 : hbarrier_3
    {
        type = "land_hbarrierwall4_f";
        resize_box[] = {0.9, 0.35, 1, 0.9, 0.9, 0.6, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class hbarrierwall6 : hbarrier_3
    {
        type = "land_hbarrierwall6_f";
        resize_box[] = {0.9, 0.35, 1, 0.9, 0.9, 0.6, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class cncwall4 : hbarrier_3
    {
        type = "land_cncwall4_f";
        resize_box[] = {0.9, 1, 1, 0.9, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class camonet_blufor_big : hbarrier_3
    {
        type = "camonet_blufor_big_f";
        resize_box[] = {1, 1, 1, 1, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
        is_hollow = 1;
    };

    class camonet_blufor : hbarrier_3
    {
        type = "camonet_blufor_f";
        resize_box[] = {1, 1, 1, 1, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
        is_hollow = 1;
    };

    class camonet_blufor_open : hbarrier_3
    {
        type = "camonet_blufor_open_f";
        resize_box[] = {1, 1, 1, 1, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
        is_hollow = 1;
    };

    class hbarriertower : hbarrier_3
    {
        type = "land_hbarriertower_f";
        resize_box[] = {0.8, 0.8, 1, 0.8, 0.4, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class bagbunker_large : hbarrier_3
    {
        type = "land_bagbunker_large_f";
        resize_box[] = {1, 1, 1, 1, 1, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };

    class bagbunker_tower : hbarrier_3
    {
        type = "land_bagbunker_tower_f";
        resize_box[] = {0.9, 0.9, 1, 0.9, 0.9, 1, __EVAL(_ZDB_CF_FIRST_CONTACT_ + _ZDB_CF_ONLY_DYNAMIC_)};
    };
    