// Assets
	class builder
    {
        name = " ";
        type = "I_G_Van_01_transport_F";
        capabilities[] = {_ZDB_CAPABIL_BUILDER_, _ZDB_CAPABIL_SUPPLY_ALL_};
        categories[] = {"builder"};
        marker_symbol = "b_support";
        marker_color = "ColorBlufor";
        max[] = {4};
        empty_timeout = -1;
        deploy_code = "CUS_FNC_ASSET_DEPLOY_BOX_1_B";
    };
	
    class car_1 : ta_infantry {type = "I_G_Quadbike_01_F"; };
	class car_2 : ta_infantry {type = "I_C_Offroad_02_unarmed_F"; };
	class mrap_1 : ta_mrap {type = "I_G_Offroad_01_armed_F"; cost = ZDB_VEH_C_ATTACK;};
    
    class heli_1 : ta_heli_transport
    {
        type = "I_C_Heli_Light_01_civil_F";
        max[] = {2};
        cost = ZDB_VEH_C_UNARMED + 1;
        deploy_types = [[10, _ZDB_DCOND_BASE_, -1, _ZDB_DTYPE_MAPCLICK_]];
        marker_symbol = "b_air";
        empty_timeout = -1;
        add_wpns[] = {
            {"CMFlareLauncher", {-1}}
        };
        add_mags[] = {
            {"168Rnd_CMFlare_Chaff_Magazine", {-1}, -1}
        };
    };
    // Asset : Assign MACE Object Type: B_Heli_Light_01_armed_F 
    class aircraft_1 : ta_plane {type = "I_C_Plane_Civil_01_F";};
    // VEHICLE_SEA : TODO MACE Object Type:  : I_C_Boat_Transport_02_F 
    // VEHICLE_SEA : TODO MACE Object Type:  : C_Boat_Civil_01_F 
    // VEHICLE_SEA : TODO MACE Object Type:  : I_C_Boat_Transport_01_F 
    // VEHICLE_SEA : TODO MACE Object Type:  : C_Scooter_Transport_01_F 
    class mortar_1
    {
        name="";
        type = "B_T_Mortar_01_F";
        resize_box[] = {0.7, 0.7, 1, 0.8, 1.5, 0.8, _ZDB_CF_FIRST_CONTACT_};
        cost = 5;
        credit_back = 0;
        max[] = {};
        marker_symbol = "b_mortar";
        deploy_types = [[10, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_DTYPE_BOX_]];
        undeploy_types = [[10, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_UTYPE_NEARBY_]];
        rem_mags[] =
        {
            {"8rnd_82mm_mo_shells", {0}},
            {"8rnd_82mm_mo_shells", {0}}
        };
        empty_timeout = ZDB_CLEAN_ABANDONED;
        respawn_time = 1200;
        service_time = 120;
        
    };
    class static_wep_1
    {
         type = "B_T_HMG_01_F";
         resize_box[] = {1, 0.4, 0, 0.8, 1, 1, _ZDB_CF_FIRST_CONTACT_};
         categories[] = {"StaticWeapon"};
         marker_symbol = "b_installation";

         deploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_DTYPE_BOX_]];
         undeploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_UTYPE_NEARBY_]];
         cost = ZDB_VEH_S_ARMED;
    };
    class static_wep_2
    {
         type = "B_T_GMG_01_F";
         resize_box[] = {1, 0.4, 0, 0.8, 1, 1, _ZDB_CF_FIRST_CONTACT_};
         categories[] = {"StaticWeapon"};
         marker_symbol = "b_installation";

         deploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_DTYPE_BOX_]];
         undeploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_UTYPE_NEARBY_]];
         cost = ZDB_VEH_S_ARMED;
    };
    class static_wep_3
    {
         type = "I_GMG_01_F";
         resize_box[] = {1, 0.4, 0, 0.8, 1, 1, _ZDB_CF_FIRST_CONTACT_};
         categories[] = {"StaticWeapon"};
         marker_symbol = "b_installation";

         deploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_DTYPE_BOX_]];
         undeploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_UTYPE_NEARBY_]];
         cost = ZDB_VEH_S_ARMED;
    };
    class static_wep_4
    {
         type = "I_GMG_01_F";
         resize_box[] = {1, 0.4, 0, 0.8, 1, 1, _ZDB_CF_FIRST_CONTACT_};
         categories[] = {"StaticWeapon"};
         marker_symbol = "b_installation";

         deploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_DTYPE_BOX_]];
         undeploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_UTYPE_NEARBY_]];
         cost = ZDB_VEH_S_ARMED;
    };
    class static_wep_5
    {
         type = "I_GMG_01_high_F";
         resize_box[] = {1, 0.4, 0, 0.8, 1, 1, _ZDB_CF_FIRST_CONTACT_};
         categories[] = {"StaticWeapon"};
         marker_symbol = "b_installation";

         deploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_DTYPE_BOX_]];
         undeploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_UTYPE_NEARBY_]];
         cost = ZDB_VEH_S_ARMED;
    };
    class static_wep_6
    {
         type = "I_static_AA_F";
         resize_box[] = {1, 0.4, 0, 0.8, 1, 1, _ZDB_CF_FIRST_CONTACT_};
         categories[] = {"StaticWeapon"};
         marker_symbol = "b_installation";

         deploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_DTYPE_BOX_]];
         undeploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_UTYPE_NEARBY_]];
         cost = ZDB_VEH_S_ARMED;
    };
    class static_wep_7
    {
         type = "I_static_AT_F";
         resize_box[] = {1, 0.4, 0, 0.8, 1, 1, _ZDB_CF_FIRST_CONTACT_};
         categories[] = {"StaticWeapon"};
         marker_symbol = "b_installation";

         deploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_DTYPE_BOX_]];
         undeploy_types = [[20, _ZDB_CAPABIL_BUILDER_, ZDB_DEPLOY_DISTANCE_AVERAGE, _ZDB_UTYPE_NEARBY_]];
         cost = ZDB_VEH_S_ARMED;
    };